// scroll from page to page 

// navigation script
$(function() {
	// push the menu on screen from left
	$('.menu-icon').click(function() {
		$('.main-nav').animate({
			left: "0%"
		}, 500);
		$('.menu-icon').css("display", "none").animate({
			display: 'none'
		}, 200);
		$('.close-icon').css("display", "block").animate({
			display: 'block'
		}, 200);
		// $('body').animate({
		// 	left: "300px"
		// }, 200);
		// media query
		if ($(window).width() < 568) {
			// change width of navigation
			$('.main-nav').css('width', '100%');
			$('.main-nav').css('top', '-150%');
			// then move navigation
			$('.main-nav').animate({
				top: "0%"
			}, 200);
			// $('body').animate({
			// left: "100%"
			// }, 200);
		};
	});
	// push menu and site back to normal
	$('.close-icon').click(function() {
		$('.main-nav').animate({
			left: "150%"
		}, 600);
		$('.menu-icon').css("display", "block").animate({
			display: 'block'
		}, 200);
		$('.close-icon').css("display", "none").animate({
			display: 'none'
		}, 200);
		// $('body').animate({
		// 	left: "0px"
		// }, 200);
		// media query
		if ($(window).width() < 568) {
			// change width of navigation
			$('.main-nav').css('width', '100%');
			$('.main-nav').css('left', '-100%');
			// then move navigation
			$('.main-nav').animate({
				left: "-100%"
			}, 200);
			$('body').animate({
			left: "0%"
			}, 200);
		};
	});
	$('a[href^="#"]').on('click',function (e) {
       e.preventDefault();

       var target = this.hash,
       $target = $(target);

       $('html, body').stop().animate({
           'scrollTop': $target.offset().top
       }, 2000, 'swing', function () {
           window.location.hash = target;
       });
   });
});