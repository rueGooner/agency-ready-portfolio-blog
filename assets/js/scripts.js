// navigation script
$(function() {
	// push the menu on screen from left
	$('.menu-icon').click(function() {
		$('.main-nav').animate({
			left: "0px"
		}, 200);
		$('body').animate({
			left: "300px"
		}, 200);
		// media query
		if ($(window).width() < 568) {
			// change width of navigation
			$('.main-nav').css('width', '100%');
			$('.main-nav').css('left', '-100%');
			// then move navigation
			$('.main-nav').animate({
				left: "0%"
			}, 200);
			$('body').animate({
			left: "100%"
			}, 200);
		};
	});
	// push menu and site back to normal
	$('.close-icon').click(function() {
		$('.main-nav').animate({
			left: "-300px"
		}, 200);
		$('body').animate({
			left: "0px"
		}, 200);
		// media query
		if ($(window).width() < 568) {
			// change width of navigation
			$('.main-nav').css('width', '100%');
			$('.main-nav').css('left', '-100%');
			// then move navigation
			$('.main-nav').animate({
				left: "-100%"
			}, 200);
			$('body').animate({
			left: "0%"
			}, 200);
		};
	});

});